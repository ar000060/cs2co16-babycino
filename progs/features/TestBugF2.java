class TestBugF2 {
    public static void main(String[] a) {
        if (new Test().f2()) {
        } else {
        }
    }
}

class Test {
    public boolean f2() {
        int testVal;
        boolean output;

        testVal = 0;

        if (true || this.g(testVal)) {
            output = false;
        } else {
            output = true;
        }

        return output;
    }

    public boolean g(int n) {
        System.out.println(n);
        return false;
    }
}
