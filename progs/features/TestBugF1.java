class TestBugF1 {
    public static void main(String[] a) {
        if (new Test().f1()) {
        } else {
        }
    }
}

class Test {
    public boolean f1() {
        boolean output;
        output = true;

        if (5 || false) {
            output = false;
        } else {
            output = true;
        }

        return output;
    }
}
